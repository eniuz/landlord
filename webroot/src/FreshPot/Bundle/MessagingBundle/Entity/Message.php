<?php

namespace FreshPot\Bundle\MessagingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Message
 *
 * @ORM\Table(name="message")
 * @ORM\Entity(repositoryClass="FreshPot\Bundle\MessagingBundle\Repository\MessageRepository")
 */
class Message
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="messageFrom", type="integer")
     */
    private $messageFrom;

    /**
     * @var int
     *
     * @ORM\Column(name="messageTo", type="integer")
     */
    private $messageTo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateSent", type="datetime")
     */
    private $dateSent;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text")
     */
    private $message;

    /**
     * @var int
     *
     * @ORM\Column(name="attachments", type="integer", nullable=true)
     */
    private $attachments;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set messageFrom
     *
     * @param integer $messageFrom
     *
     * @return Message
     */
    public function setMessageFrom($messageFrom)
    {
        $this->messageFrom = $messageFrom;

        return $this;
    }

    /**
     * Get messageFrom
     *
     * @return int
     */
    public function getMessageFrom()
    {
        return $this->messageFrom;
    }

    /**
     * Set messageTo
     *
     * @param integer $messageTo
     *
     * @return Message
     */
    public function setMessageTo($messageTo)
    {
        $this->messageTo = $messageTo;

        return $this;
    }

    /**
     * Get messageTo
     *
     * @return int
     */
    public function getMessageTo()
    {
        return $this->messageTo;
    }

    /**
     * Set dateSent
     *
     * @param \DateTime $dateSent
     *
     * @return Message
     */
    public function setDateSent($dateSent)
    {
        $this->dateSent = $dateSent;

        return $this;
    }

    /**
     * Get dateSent
     *
     * @return \DateTime
     */
    public function getDateSent()
    {
        return $this->dateSent;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return Message
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set attachments
     *
     * @param integer $attachments
     *
     * @return Message
     */
    public function setAttachments($attachments)
    {
        $this->attachments = $attachments;

        return $this;
    }

    /**
     * Get attachments
     *
     * @return int
     */
    public function getAttachments()
    {
        return $this->attachments;
    }
}


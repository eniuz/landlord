#!/bin/bash

export SYMFONY_ENV=prod;
composer install --no-dev --optimize-autoloader;
chmod +x composer.phar;
php composer.phar update;
chmod 777 -R var/logs/;
chmod 777 -R var/cache/;
export SYMFONY_ENV=dev
bin/console server:run;
